"use strict"
const AWS = require("aws-sdk")
const util = require("util")
const sharp = require("sharp")
const s3 = new AWS.S3()

const SRC_FOLDER = "public/loadncrunch/"
const DEST_FOLDER = "public/loadncrunchprocessed/"

const sizes = [20, 50, 100, 200, 500, 700, 1200, 1800]

function log(...args) {
  console.log(...args)
}

module.exports.handler = async event => {
  const { s3: s3Obj } = event.Records[0]

  if (
    s3Obj.object.key.indexOf(SRC_FOLDER) !== 0 ||
    s3Obj.object.key.indexOf("processed") > -1
  ) {
    return
  }
  log("Reading options from event:\n", util.inspect(event, { depth: 5 }))

  const srcBucket = s3Obj.bucket.name
  const srcKey = s3Obj.object.key
  const absoluteImagePath = `${srcBucket}/${srcKey}`
  const destKey = s3Obj.object.key.replace(/^.*[\\\/]/, "")

  log(`Getting image from S3: ${absoluteImagePath}`)
  const response = await s3
    .getObject({ Bucket: srcBucket, Key: srcKey })
    .promise()

  for (const size of sizes) {
    log(`Resizing image to size: ${size}`)

    const resizedImage = await sharp(response.Body)
      .resize(size)
      .toBuffer()

    log(`Uploading processed image to: ${srcKey}`)
    await s3
      .putObject({
        Bucket: srcBucket,
        Key: `${DEST_FOLDER}${size}/${destKey}`,
        Body: resizedImage,
        ContentType: response.ContentType,
      })
      .promise()
  }
  log(`Writting processed file for ${srcBucket}/${srcKey}`)

  await s3.putObject({
    Bucket: DEST_FOLDER,
    Key: srcKey + ".processed",
    Body: "processed",
  })
  log(`Successfully processed ${srcBucket}/${srcKey}`)
}

// eslint-disable-next-line
// exports.handler = async function(event, context) {
//   console.log('Received S3 event:', JSON.stringify(event, null, 2));
//   // Get the object from the event and show its content type
//   const bucket = event.Records[0].s3.bucket.name; //eslint-disable-line
//   const key = event.Records[0].s3.object.key; //eslint-disable-line
//   console.log(`Bucket: ${bucket}`, `Key: ${key}`);
//   context.done(null, 'Successfully processed S3 event'); // SUCCESS with message
// };
