import React, { useEffect, useState } from "react"
import { Link } from "gatsby"
import Amplify, { I18n, Storage } from "aws-amplify"
import awsConfig from "../aws-exports"

import Layout from "../components/layout"
import SEO from "../components/seo"
import { PhotoPicker, AmplifyTheme, S3Album } from "aws-amplify-react"
import { Spin, Button } from "antd"
const sizes = [20, 50, 100, 200, 500, 700]

const SecondPage = () => {
  const [loadingImg, setLoadingImage] = useState(false)
  const [uploading, setUploading] = useState(false)
  const [uploaded, setUploaded] = useState(false)
  const [images, setImages] = useState([])
  const [file, setFile] = useState()

  useEffect(() => {
    Amplify.configure(awsConfig)
  }, [])

  function getNextImage(){
    if(sizes.length>0){
      console.log('Getting image',sizes[0])
      Storage.get(`loadncrunchprocessed/${sizes[0]}/test3.png`)
        .then(url=>{
          console.log('GOT',url)
          images.push(url)
          setImages(images)
          sizes.splice(0,1)
          getNextImage()
        })
        .catch(e=>{
          console.log(`Got Error on size ${sizes[0]}`)
          setTimeout(getNextImage,1000)
        })
    }
  }
  useEffect(()=>{
    if(uploaded)
      getNextImage()
  },[uploaded])

  function uploadImg() {
    setUploading(true)
    Storage.put("loadncrunch/test3.png", file, {
      contentType: file.type,
    })
      .then(r => {
        console.log("Listo", r)
        setUploaded(true)
      })
      .catch(e => {
        console.log("Hubo un error", e)
      })
  }
  return (
    <Layout>
      <SEO title="S3 Cruncher" />
      <h1>Upload n' Crunch</h1>

      <div style={{ display: "flex", justifyContent: "space-between" }}>
        <input
          type="file"
          accept="image/png"
          onChange={e => {
            setLoadingImage(true)
            setFile(e.target.files[0])
          }}
        />
        <div style={{ width: "100%" }}>
          {file && (
            <div style={{ textAlign: "center", margin: "1em" }}>
              <Button
                type="primary"
                icon="upload"
                size="large"
                onClick={uploadImg}
              >
                Upload n'Crunch {Math.round(file.size / 1024)}KB
              </Button>
            </div>
          )}
        </div>
        
      </div>
      <div
          style={{
            transition: "width ease-in 0.5s",
            margin: "0 1em",
            width: uploading ? "100%" : "0px",
            overflow: "hidden",
          }}
        >
          {images.map(url=><img key={url} src={url} />)}
        </div>
    </Layout>
  )
}

export default SecondPage
